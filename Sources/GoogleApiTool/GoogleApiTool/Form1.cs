﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

//This file was created by Ivan Mazur in April 2015////////////////////////////////////////////////// 
//Here I handle events from UI///////////////////////////////////////////////////////////////////////

namespace GoogleApiTool
{
    public partial class Form1 : Form
    {
        WorkWithHttp HttpRequest = new WorkWithHttp();

        public Form1()
        {
            InitializeComponent();

        }

        //Language picker.I use google reductions.

       

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

            if (comboBox1.Text == "Russian(ru)")     HttpRequest.SetLanguage("ru");
            if (comboBox1.Text == "Ukrainian(uk)")   HttpRequest.SetLanguage("uk");
            if (comboBox1.Text == "German(de)")      HttpRequest.SetLanguage("de");
            if (comboBox1.Text == "English(en)")     HttpRequest.SetLanguage("en");
            if (comboBox1.Text == "Spanish(es)")     HttpRequest.SetLanguage("es");
            if (comboBox1.Text == "French(fr)")      HttpRequest.SetLanguage("fr");
            if (comboBox1.Text == "Italian(it)")     HttpRequest.SetLanguage("it");
            if (comboBox1.Text == "Japanese(ja)")    HttpRequest.SetLanguage("ja");
            if (comboBox1.Text == "Portuguese(pt)")  HttpRequest.SetLanguage("pt");

        }

        //Handling Start button event

        private void button1_Click(object sender, EventArgs e)
        {
            string strInputPath = System.IO.Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Input data.xlsx");
            string strOutputPath = System.IO.Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Output data.xlsx");
            string strReqPath = System.IO.Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Request.xml");
            DateTime time = DateTime.Now;
            DateTime sectime = new DateTime(1970, 1, 1, 1,1,1);
            double diff = (time - sectime).TotalHours;

            WorkWithExcel ExcelInput = new WorkWithExcel(@strInputPath);
            WorkWithExcel ExcelOutput = new WorkWithExcel(@strOutputPath);
            XmlParser xmlPrs = new XmlParser(@strReqPath);
           
            //Getting number of columns in excel file
            int ColumnNumber = 0;
            ColumnNumber= ExcelInput.GetCellNumberInColumn(1);
            
            
            string GoogleResponse = null;
            string longtitude = null;
            string latitude = null;

            //If you dont want google to block your ip, you `ll better not to push more than 2500 requests per 24 hours

            if (ColumnNumber > 2450) ColumnNumber = 2450;

            for (int i = 1; i <= ColumnNumber; i++)
            {
                //getting coordinates from file
                latitude = ExcelInput.GetCellValue(i, 1); listBox1.Items.Add("Longtitude get: " + longtitude);
                longtitude = ExcelInput.GetCellValue(i, 2);   listBox1.Items.Add("Latitude get: "+ latitude);

                // getting the response data
                HttpRequest.SetCoordinates(latitude,longtitude);
                GoogleResponse=HttpRequest.GetHttpResponse();

                //write all xml data from google response into the file
                System.IO.File.WriteAllText(@strReqPath, GoogleResponse);

                //parse data
                 xmlPrs.ParseData();

                //Save parsed data in file
                 ExcelOutput.SetCellValue(xmlPrs.GetAdminLevel1(), i, 4);
                 ExcelOutput.SetCellValue(xmlPrs.GetAdminLevel2(), i, 3);
                 ExcelOutput.SetCellValue(xmlPrs.GetLocality(), i, 2);
                 ExcelOutput.SetCellValue(xmlPrs.GetCountry(), i, 1);
                 listBox1.Items.Add(xmlPrs.GetLocality());
            
            }
                //save and close the files
                 ExcelOutput.Save();
                 ExcelOutput.Close();
                 ExcelInput.Close();

                 listBox1.Items.Add("Successfully finished all processes! ");
        }
    }
}
