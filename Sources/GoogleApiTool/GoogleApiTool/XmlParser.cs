﻿using System.Xml.Linq;

//This file was created by Ivan Mazur in April 2015////////////////////////////////////////////////// 
//I use this class for parsing the required data from xml file
namespace GoogleApiTool
{
    class XmlParser
    {

        private string m_admin_area_level_1 = null;
        private string m_admin_area_level_2 = null;
        private string m_locality = null;
        private string m_country = null;
        private string m_PathToFile = null;

        //class designer
       public XmlParser(string PathToFile)
        {

            m_PathToFile = PathToFile;

        }

        //parsing data from file
        public void ParseData()
        {

            XDocument xdoc = XDocument.Load(m_PathToFile);

            foreach (XElement adressElement in xdoc.Element("GeocodeResponse").Elements("result"))
            {

                XElement LocalizedNameElement = adressElement.Element("formatted_address");

                XElement TypeElement = adressElement.Element("type");

                XElement NameElement = adressElement.Element("address_component").Element("long_name");

                if (NameElement != null)
                {

                    if (TypeElement.Value == "administrative_area_level_1")
                        m_admin_area_level_1 = NameElement.Value;

                    if (TypeElement.Value == "administrative_area_level_2")
                        m_admin_area_level_2 = NameElement.Value;

                    if (TypeElement.Value == "locality")
                        m_locality = NameElement.Value;

                    if (TypeElement.Value == "country")
                        m_country = NameElement.Value;

                }


            }
        
        }

        //methods for getting separeted adress values

        public string GetAdminLevel1()
        {

            return m_admin_area_level_1;

        }

        public string GetAdminLevel2()
        {

            return m_admin_area_level_2;

        }

        public string GetLocality()
        {
            
            return m_locality;

        }

        public string GetCountry()
        {

            return m_country;

        }

    }
}
