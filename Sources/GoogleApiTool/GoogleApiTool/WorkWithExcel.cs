﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Excel = Microsoft.Office.Interop.Excel;

//This file was created by Ivan Mazur in April 2015////////////////////////////////////////////////// 
//I use this class for working with excel files//////////////////////////////////////////////////////

namespace GoogleApiTool
{
    class WorkWithExcel
    {
        private Excel._Application m_Application = null;
        private Excel.Workbook m_Workbook = null;
        private Excel.Worksheet m_WorkSheet = null;
        private object m_MissingObject = System.Reflection.Missing.Value;

        //class designer
        public WorkWithExcel()
        {

            m_Application = new Excel.Application();
            m_Workbook = new Excel.Workbook();
            m_WorkSheet = (Excel.Worksheet)m_Workbook.Worksheets.get_Item(1);

        }

        public WorkWithExcel(string PathToFile)
        {

            object PathToFileObject = PathToFile;

            m_Application = new Excel.Application();
            m_Workbook = m_Application.Workbooks.Add(PathToFileObject);
            m_WorkSheet = (Excel.Worksheet)m_Workbook.Worksheets.get_Item(1);

        }

        //the option of excel application visibility
        public bool Visible
        {

            get { return m_Application.Visible; }
            set { m_Application.Visible = value; }

        }

        //setting value for excel file`s cell with such indexes
        public void SetCellValue(string NewValue, int RowIndex, int ColumnIndex)
        {

            m_WorkSheet.Cells[RowIndex, ColumnIndex] = NewValue;

        }

        //getting cell value from excel file with such indexes
        public string GetCellValue(int RowIndex, int ColumnIndex)
        {

            string RequiredValue = null;

            RequiredValue = m_Application.Cells[RowIndex, ColumnIndex].Value.ToString();

            return RequiredValue;

        }

        //getting the cells number in this column
        public int GetCellNumberInColumn(int ColumnIndex)
        {
            Excel.Range xlRange = m_WorkSheet.UsedRange;
            int RowCount = xlRange.Rows.Count;

            return RowCount;

        }

        //saving the file
        public void Save()
        {
            string strOutputPath = System.IO.Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Output data.xlsx");
            m_Workbook.SaveCopyAs(@strOutputPath);
        
        }

        //closing and desposing all used data 
        public void Close()
        {

            m_Workbook.Close(false, m_MissingObject, m_MissingObject);

            m_Application.Quit();

            System.Runtime.InteropServices.Marshal.ReleaseComObject(m_Application);

            m_Application = null;
            m_Workbook = null;
            m_WorkSheet = null;

            System.GC.Collect();

        }


    }
}
