﻿namespace CoordinatesToCityNameLocalised
{
    partial class Single_Request
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Longtitude = new System.Windows.Forms.TextBox();
            this.ClearBut = new System.Windows.Forms.Button();
            this.StartBut = new System.Windows.Forms.Button();
            this.Latitude = new System.Windows.Forms.TextBox();
            this.longtitude_label = new System.Windows.Forms.Label();
            this.latitude_label = new System.Windows.Forms.Label();
            this.LogList = new System.Windows.Forms.ListBox();
            this.LanguageSelector = new System.Windows.Forms.ComboBox();
            this.Language_label = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // Longtitude
            // 
            this.Longtitude.Location = new System.Drawing.Point(143, 25);
            this.Longtitude.Name = "Longtitude";
            this.Longtitude.Size = new System.Drawing.Size(124, 20);
            this.Longtitude.TabIndex = 0;
            // 
            // ClearBut
            // 
            this.ClearBut.Location = new System.Drawing.Point(12, 99);
            this.ClearBut.Name = "ClearBut";
            this.ClearBut.Size = new System.Drawing.Size(124, 39);
            this.ClearBut.TabIndex = 2;
            this.ClearBut.Text = "Clear";
            this.ClearBut.UseVisualStyleBackColor = true;
            this.ClearBut.Click += new System.EventHandler(this.ClearBut_Click);
            // 
            // StartBut
            // 
            this.StartBut.Location = new System.Drawing.Point(138, 99);
            this.StartBut.Name = "StartBut";
            this.StartBut.Size = new System.Drawing.Size(129, 39);
            this.StartBut.TabIndex = 3;
            this.StartBut.Text = "Start";
            this.StartBut.UseVisualStyleBackColor = true;
            this.StartBut.Click += new System.EventHandler(this.StartBut_Click);
            // 
            // Latitude
            // 
            this.Latitude.Location = new System.Drawing.Point(10, 25);
            this.Latitude.Name = "Latitude";
            this.Latitude.Size = new System.Drawing.Size(124, 20);
            this.Latitude.TabIndex = 4;
            // 
            // longtitude_label
            // 
            this.longtitude_label.AutoSize = true;
            this.longtitude_label.Location = new System.Drawing.Point(140, 9);
            this.longtitude_label.Name = "longtitude_label";
            this.longtitude_label.Size = new System.Drawing.Size(57, 13);
            this.longtitude_label.TabIndex = 5;
            this.longtitude_label.Text = "Longtitude";
            // 
            // latitude_label
            // 
            this.latitude_label.AutoSize = true;
            this.latitude_label.Location = new System.Drawing.Point(10, 9);
            this.latitude_label.Name = "latitude_label";
            this.latitude_label.Size = new System.Drawing.Size(45, 13);
            this.latitude_label.TabIndex = 6;
            this.latitude_label.Text = "Latitude";
            // 
            // LogList
            // 
            this.LogList.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.LogList.FormattingEnabled = true;
            this.LogList.Location = new System.Drawing.Point(12, 144);
            this.LogList.Name = "LogList";
            this.LogList.Size = new System.Drawing.Size(255, 108);
            this.LogList.TabIndex = 7;
            // 
            // LanguageSelector
            // 
            this.LanguageSelector.FormattingEnabled = true;
            this.LanguageSelector.Items.AddRange(new object[] {
            "English(en)",
            "Russian(ru)",
            "Ukrainian(uk)",
            "German(de)",
            "Spanish(es)",
            "French(fr)",
            "Italian(it)",
            "Japanese(ja)",
            "Portuguese(pt)"});
            this.LanguageSelector.Location = new System.Drawing.Point(13, 72);
            this.LanguageSelector.Name = "LanguageSelector";
            this.LanguageSelector.Size = new System.Drawing.Size(254, 21);
            this.LanguageSelector.TabIndex = 8;
            this.LanguageSelector.SelectedIndexChanged += new System.EventHandler(this.LanguageSelector_SelectedIndexChanged);
            // 
            // Language_label
            // 
            this.Language_label.AutoSize = true;
            this.Language_label.Location = new System.Drawing.Point(15, 53);
            this.Language_label.Name = "Language_label";
            this.Language_label.Size = new System.Drawing.Size(88, 13);
            this.Language_label.TabIndex = 9;
            this.Language_label.Text = "Select Language";
            // 
            // Single_Request
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(279, 261);
            this.Controls.Add(this.Language_label);
            this.Controls.Add(this.LanguageSelector);
            this.Controls.Add(this.LogList);
            this.Controls.Add(this.latitude_label);
            this.Controls.Add(this.longtitude_label);
            this.Controls.Add(this.Latitude);
            this.Controls.Add(this.StartBut);
            this.Controls.Add(this.ClearBut);
            this.Controls.Add(this.Longtitude);
            this.MaximumSize = new System.Drawing.Size(295, 500);
            this.Name = "Single_Request";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Single_Request";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox Longtitude;
        private System.Windows.Forms.Button ClearBut;
        private System.Windows.Forms.Button StartBut;
        private System.Windows.Forms.TextBox Latitude;
        private System.Windows.Forms.Label longtitude_label;
        private System.Windows.Forms.Label latitude_label;
        private System.Windows.Forms.ListBox LogList;
        private System.Windows.Forms.ComboBox LanguageSelector;
        private System.Windows.Forms.Label Language_label;
    }
}