﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CoordinatesToCityNameLocalised
{
    public partial class Single_Request : Form
    {
        //creating HttpManager class object
        HttpManager HttpRequest = new HttpManager();

        public Single_Request()
        {
            InitializeComponent();
            LanguageSelector.Text = LanguageSelector.Items[0].ToString();
        }
        /// <summary>
        /// start button
        /// sending the request, parsing the response and return the required value
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void StartBut_Click(object sender, EventArgs e)
        {
            try
            {
                //clearing the list before starting viewing the log
                LogList.Items.Clear();

                //path to xml file,which contains the request data
                string strReqPath = System.IO.Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Request.xml");


                XmlParser xmlPrs = new XmlParser(@strReqPath);

                //some variables?whicj we need to make a request
                string GoogleResponse = string.Empty;
                string longtitude = string.Empty;
                string latitude = string.Empty;

                //marker
                bool TheDataIsOk=true;

                // looking for troubles
                if (Longtitude.Text != string.Empty)
                {
                    longtitude = Longtitude.Text;
                }
                else
                {
                    MessageBox.Show("Longtitude value does not exist");
                    TheDataIsOk=false;
                }

                if (Latitude.Text != string.Empty)
                {
                    latitude = Latitude.Text;
                }
                else
                {
                    MessageBox.Show("Latitude value does not exist");
                    TheDataIsOk=false;
                }

                if (LanguageSelector.Text == string.Empty)
                {
                    MessageBox.Show("Locale value does not exist");
                    TheDataIsOk=false;
                }

                if (TheDataIsOk)
                {
                    //setting some request values
                    HttpRequest.SetLatitude = latitude;
                    HttpRequest.SetLongtitude = longtitude;

                    //error message
                    string Error=string.Empty;

                    //sending the http request and getting the response
                    HttpRequest.GetHttpResponse(out Error, out GoogleResponse);

                    if (Error != string.Empty)
                    {
                     MessageBox.Show(Error);
                    }

                    //writing the response into xml file
                    System.IO.File.WriteAllText(@strReqPath, GoogleResponse);

                    //parsing the xml file
                    xmlPrs.ParseData(out Error);

                    if (Error != string.Empty)
                    {
                        MessageBox.Show(Error);
                    }

                    //writing the data into list
                    LogList.Items.Add("Administrative level 1 :");
                    LogList.Items.Add(xmlPrs.GetAdminLevel1);

                    LogList.Items.Add("Administrative level 2 :");
                    LogList.Items.Add(xmlPrs.GetAdminLevel2);

                    LogList.Items.Add("Country name :");
                    LogList.Items.Add(xmlPrs.GetCountry);

                    LogList.Items.Add("Locality:");
                    LogList.Items.Add(xmlPrs.GetLocality);
                        
                }

                

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);

            }
            finally 
            {
                LogList.Items.Add("Successfully finished all processes!");
            }
        }

        /// <summary>
        /// if selected index changed than setting the http request local value
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void LanguageSelector_SelectedIndexChanged(object sender, EventArgs e)
        {

            if (LanguageSelector.Text == "Russian(ru)") HttpRequest.SetLocale = "ru";
            if (LanguageSelector.Text == "Ukrainian(uk)") HttpRequest.SetLocale="uk";
            if (LanguageSelector.Text == "German(de)") HttpRequest.SetLocale="de";
            if (LanguageSelector.Text == "English(en)") HttpRequest.SetLocale="en";
            if (LanguageSelector.Text == "Spanish(es)") HttpRequest.SetLocale="es";
            if (LanguageSelector.Text == "French(fr)") HttpRequest.SetLocale="fr";
            if (LanguageSelector.Text == "Italian(it)") HttpRequest.SetLocale="it";
            if (LanguageSelector.Text == "Japanese(ja)") HttpRequest.SetLocale="ja";
            if (LanguageSelector.Text == "Portuguese(pt)") HttpRequest.SetLocale="pt";

        }

        /// <summary>
        /// clear button clicked
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ClearBut_Click(object sender, EventArgs e)
        {
            //clearing the controls
            LanguageSelector.Text = string.Empty;
            LogList.Items.Clear();
            Longtitude.Text = string.Empty;
            Latitude.Text = string.Empty;
        }
    }
}
