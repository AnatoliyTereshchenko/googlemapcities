﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Http;
using System.Net;
using System.IO;

//This file was created by Ivan Mazur in May(31) 2015 2.42am ////////////////////////////////////////////////// 
//I use this class for genering and handling the http requests/////////////////////////////////////////

namespace CoordinatesToCityNameLocalised
{
    class HttpManager
    {

        private string m_locale = string.Empty;
        private string m_longtitude = string.Empty;
        private string m_latitude = string.Empty;

        /// <summary>
        /// setting the longtitude value
        /// </summary>
        public string SetLongtitude
        {
            get { return m_longtitude; }
            set { m_longtitude = value; }
        }

        /// <summary>
        /// setting the latitude value
        /// </summary>
        public string SetLatitude
        {
            get { return m_latitude; }
            set { m_latitude = value; }
        }

        /// <summary>
        /// setting the locale
        /// </summary>
        public string SetLocale
        {
            get { return m_locale; }
            set { m_locale = value; }
        }

        /// <summary>
        /// here we sending the http request and getting the response as a string value
        /// </summary>
        /// <param name="ErrorMsg"></param>
        /// <param name="Response"></param>
        /// <returns></returns>
        public bool GetHttpResponse(out string ErrorMsg, out string Response)
        {
            ErrorMsg = string.Empty;
            Response = string.Empty;

            try
            {
                string error = string.Empty;
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(GenerateRequest(out error));

                //several request settings 
                request.MaximumAutomaticRedirections = 1;
                request.MaximumResponseHeadersLength = 1;

                //setting the credentials
                request.Credentials = CredentialCache.DefaultCredentials;
                //getting the response
                HttpWebResponse response = (HttpWebResponse)request.GetResponse();

                Stream receiveStream = response.GetResponseStream();

                StreamReader readStream = new StreamReader(receiveStream, Encoding.UTF8);

                //getting the string with response data
                Response = readStream.ReadToEnd();

                response.Close();
                readStream.Close();
            }
            catch(Exception ex)
            {
                ErrorMsg = ex.Message;
                return false;
            }
            return true;
        }

        /// <summary>
        /// generating an http request string
        /// </summary>
        /// <param name="ErrorMsg"></param>
        /// <returns></returns>
        private string GenerateRequest(out string ErrorMsg)
        {
            ErrorMsg = string.Empty;
            string request = string.Empty;

            try
            {
                string sample = "http://maps.googleapis.com/maps/api/geocode/xml?latlng=";

                sample += m_longtitude;
                sample += ",";
                sample += m_latitude;
                sample += "&language=";
                sample += m_locale;
                sample += "&sensor=true_or_false";

                request = sample;
            }
            catch (Exception ex)
            {
                ErrorMsg = ex.Message;
            }
            return request;
        }



    }
}
