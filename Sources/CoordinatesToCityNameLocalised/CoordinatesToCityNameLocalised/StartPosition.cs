﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CoordinatesToCityNameLocalised
{
    public partial class StartPosition : Form
    {
        /// <summary>
        /// class designer
        /// </summary>
        public StartPosition()
        {
            InitializeComponent();
            label1.Text = "Start position is:" + MainForm.StartPositionInFile;
        }

        /// <summary>
        /// Cancel button implementation
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CancelButSP_Click(object sender, EventArgs e)
        {
            try
            {
                //setting the cancel value for dialog
                this.DialogResult = DialogResult.Cancel;
            }

            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

            finally
            {
                //closing the form
                this.Close();
            }
        }

       
        /// <summary>
        /// Make the textbox only for numeric values
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void textBox1_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((e.KeyChar <= 47 || e.KeyChar >= 58) && e.KeyChar != 8)
            {
                e.Handled = true;
            }
              

         }

        /// <summary>
        /// Showing the current value on the form
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ChangePosTextBoxSP_TextChanged(object sender, EventArgs e)
        {

            label1.Text = "Start position is:" + ChangePosTextBoxSP.Text;

        }

        /// <summary>
        /// accept button implementation
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void AcceptButSP_Click(object sender, EventArgs e)
        {
            try
            {
                // changing the MainForm variable
                MainForm.StartPositionInFile = ChangePosTextBoxSP.Text;

                //setting the dialog result value
                this.DialogResult = DialogResult.OK;
            }

            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

            finally
            {
                //closing tha form
                this.Close();
            }
        
       
        }
       

    }
}
