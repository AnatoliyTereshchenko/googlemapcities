﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;



namespace CoordinatesToCityNameLocalised
{

    public partial class MainForm : Form
    {
        public static string StartPositionInFile = "1";

        HttpManager HttpRequest = new HttpManager();

        public MainForm()
        {

            InitializeComponent();
            LanguagePicker.Text = LanguagePicker.Items[0].ToString();
        }

        private void LanguagePicker_SelectedIndexChanged(object sender, EventArgs e)
        {

            if (LanguagePicker.Text == "Russian(ru)") HttpRequest.SetLocale = "ru";
            if (LanguagePicker.Text == "Ukrainian(uk)") HttpRequest.SetLocale="uk";
            if (LanguagePicker.Text == "German(de)") HttpRequest.SetLocale="de";
            if (LanguagePicker.Text == "English(en)") HttpRequest.SetLocale="en";
            if (LanguagePicker.Text == "Spanish(es)") HttpRequest.SetLocale="es";
            if (LanguagePicker.Text == "French(fr)") HttpRequest.SetLocale="fr";
            if (LanguagePicker.Text == "Italian(it)") HttpRequest.SetLocale="it";
            if (LanguagePicker.Text == "Japanese(ja)") HttpRequest.SetLocale="ja";
            if (LanguagePicker.Text == "Portuguese(pt)") HttpRequest.SetLocale="pt";

        }

        private void button1_Click(object sender, EventArgs e)
        {
            Single_Request singForm = new Single_Request();
            singForm.Show();

        }

        private void MainForm_Load(object sender, EventArgs e)
        {

        }

        private void TransformBut_Click(object sender, EventArgs e)
        {
            StartPosition strPos = new StartPosition();

            strPos.DialogResult = DialogResult.OK;

            if (strPos.ShowDialog(this) == DialogResult.OK)
                MessageBox.Show("dd");
            else MessageBox.Show("oh no!");
        }
    }
}
