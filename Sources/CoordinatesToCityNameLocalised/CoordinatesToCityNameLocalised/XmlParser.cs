﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using System.Xml;

//This file was created by Ivan Mazur in June 2015 ////////////////////////////////////////////////// 
//I use this class for parsing the required data from xml file

namespace CoordinatesToCityNameLocalised
{
    class XmlParser
    {
        private string m_admin_area_level_1 = null;
        private string m_admin_area_level_2 = null;
        private string m_locality = null;
        private string m_country = null;
        private string m_PathToFile = null;

        /// <summary>
        /// class designer with path to file whicj we going to parse
        /// </summary>
        /// <param name="PathToFile"></param>
        public XmlParser(string PathToFile)
        {

            m_PathToFile = PathToFile;

        }

        /// <summary>
        /// parsing the required data
        /// </summary>
        /// <param name="ErrorMsg"></param>
        /// <returns></returns>
        public bool ParseData(out string ErrorMsg)
        {
            ErrorMsg = string.Empty;

            try
            {

                XDocument xdoc = XDocument.Load(m_PathToFile);

                foreach (XElement adressElement in xdoc.Element("GeocodeResponse").Elements("result"))
                {

                    XElement LocalizedNameElement = adressElement.Element("formatted_address");

                    XElement TypeElement = adressElement.Element("type");

                    XElement NameElement = adressElement.Element("address_component").Element("long_name");

                    if (NameElement != null)
                    {

                        if (TypeElement.Value == "administrative_area_level_1")
                            m_admin_area_level_1 = NameElement.Value;

                        if (TypeElement.Value == "administrative_area_level_2")
                            m_admin_area_level_2 = NameElement.Value;

                        if (TypeElement.Value == "locality")
                            m_locality = NameElement.Value;

                        if (TypeElement.Value == "country")
                            m_country = NameElement.Value;

                    }


                }

            }

            catch (Exception ex)
            {
                ErrorMsg = ex.Message;
                return false;
            }
            return true;
            
        }

        /// <summary>
        /// getting the administrative level 1
        /// </summary>
        public string GetAdminLevel1
        {
            get { return m_admin_area_level_1; }
            set { m_admin_area_level_1 = value; }
        }

        /// <summary>
        /// getting the administrative level 2
        /// </summary>
        public string GetAdminLevel2
        {
            get { return m_admin_area_level_2; }
            set { m_admin_area_level_2 = value; }
        }

        /// <summary>
        /// getting the locality
        /// </summary>
        public string GetLocality
        {
            get { return m_locality; }
            set { m_locality = value; }
            
        }

        /// <summary>
        /// getting the country
        /// </summary>
        public string GetCountry
        {
            get { return m_country; }
            set { m_country = value; }
            

        }


    }
}
