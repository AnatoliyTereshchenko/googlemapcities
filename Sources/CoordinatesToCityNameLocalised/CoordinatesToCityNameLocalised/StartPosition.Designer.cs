﻿namespace CoordinatesToCityNameLocalised
{
    partial class StartPosition
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.CancleButSP = new System.Windows.Forms.Button();
            this.AcceptButSP = new System.Windows.Forms.Button();
            this.ChangePosTextBoxSP = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.25F);
            this.label1.Location = new System.Drawing.Point(54, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(172, 20);
            this.label1.TabIndex = 0;
            this.label1.Text = "Start position is: 1234";
            this.label1.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // CancleButSP
            // 
            this.CancleButSP.Location = new System.Drawing.Point(16, 88);
            this.CancleButSP.Name = "CancleButSP";
            this.CancleButSP.Size = new System.Drawing.Size(123, 63);
            this.CancleButSP.TabIndex = 1;
            this.CancleButSP.Text = "Cancel";
            this.CancleButSP.UseVisualStyleBackColor = true;
            this.CancleButSP.Click += new System.EventHandler(this.CancelButSP_Click);
            // 
            // AcceptButSP
            // 
            this.AcceptButSP.Location = new System.Drawing.Point(145, 88);
            this.AcceptButSP.Name = "AcceptButSP";
            this.AcceptButSP.Size = new System.Drawing.Size(127, 63);
            this.AcceptButSP.TabIndex = 2;
            this.AcceptButSP.Text = "Accept";
            this.AcceptButSP.UseVisualStyleBackColor = true;
            this.AcceptButSP.Click += new System.EventHandler(this.AcceptButSP_Click);
            // 
            // ChangePosTextBoxSP
            // 
            this.ChangePosTextBoxSP.Location = new System.Drawing.Point(16, 62);
            this.ChangePosTextBoxSP.Name = "ChangePosTextBoxSP";
            this.ChangePosTextBoxSP.Size = new System.Drawing.Size(256, 20);
            this.ChangePosTextBoxSP.TabIndex = 3;
            this.ChangePosTextBoxSP.TextChanged += new System.EventHandler(this.ChangePosTextBoxSP_TextChanged);
            this.ChangePosTextBoxSP.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBox1_KeyPress);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(16, 43);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(106, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "Change start position";
            // 
            // StartPosition
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 163);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.ChangePosTextBoxSP);
            this.Controls.Add(this.AcceptButSP);
            this.Controls.Add(this.CancleButSP);
            this.Controls.Add(this.label1);
            this.Name = "StartPosition";
            this.Text = "StartPosition";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button CancleButSP;
        private System.Windows.Forms.Button AcceptButSP;
        private System.Windows.Forms.TextBox ChangePosTextBoxSP;
        private System.Windows.Forms.Label label2;
    }
}