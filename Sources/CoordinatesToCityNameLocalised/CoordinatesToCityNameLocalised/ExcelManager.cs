﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Excel = Microsoft.Office.Interop.Excel;

//This file was created by Ivan Mazur in May(30) 2015 11pm ////////////////////////////////////////////////
//I use this class for handling the excel files//////////////////////////////////////////////////////

namespace CoordinatesToCityNameLocalized
{
    class ExcelManager
    {

        private Excel._Application m_Application = null;
        private Excel.Workbook m_Workbook = null;
        private Excel.Worksheet m_WorkSheet = null;
        private string m_strOutputPath = string.Empty;
        private object m_MissingObject = System.Reflection.Missing.Value;

        /// <summary>
        /// class designer
        /// </summary>
        public ExcelManager()
        {

            m_Application = new Excel.Application();
            m_Workbook = new Excel.Workbook();
            m_WorkSheet = (Excel.Worksheet)m_Workbook.Worksheets.get_Item(1);

        }

        /// <summary>
        /// class designer with input parametr PathToFile
        /// </summary>
        /// <param name="PathToFile"></param>
        public ExcelManager(string PathToFile)
        {

            object PathToFileObject = PathToFile;

            m_Application = new Excel.Application();
            m_Workbook = m_Application.Workbooks.Add(PathToFileObject);
            m_WorkSheet = (Excel.Worksheet)m_Workbook.Worksheets.get_Item(1);

        }

        /// <summary>
        /// setting the output path
        /// </summary>
        public string SetOutputPath
        {
            get { return m_strOutputPath; }
            set { m_strOutputPath = value; }
        }

        /// <summary>
        /// the option of excel application visibility
        /// </summary>
        public bool Visible
        {

            get { return m_Application.Visible; }
            set { m_Application.Visible = value; }

        }

        /// <summary>
        /// setting the exel file cell with such indexes
        /// </summary>
        /// <param name="ErrorMsg"></param>
        /// <param name="NewValue"></param>
        /// <param name="RowIndex"></param>
        /// <param name="ColumnIndex"></param>
        /// <returns></returns>
        public bool SetCellValue(out string ErrorMsg, string NewValue, int RowIndex, int ColumnIndex)
        {
            ErrorMsg = string.Empty;

            try
            {

                m_WorkSheet.Cells[RowIndex, ColumnIndex] = NewValue;


            }

            catch (Exception ex)
            {
                ErrorMsg = ex.Message;
                return false;
            }

            return true;
        }

        /// <summary>
        /// getting the cell value with such indexes from excel file
        /// </summary>
        /// <param name="ErrorMsg"></param>
        /// <param name="CellValue"></param>
        /// <param name="RowIndex"></param>
        /// <param name="ColumnIndex"></param>
        /// <returns></returns>
        public bool GetCellValue(out string ErrorMsg, out string CellValue, int RowIndex, int ColumnIndex)
        {
            ErrorMsg = string.Empty;

            CellValue = string.Empty;

            try
            {

                CellValue = m_Application.Cells[RowIndex, ColumnIndex].Value.ToString();

            }

            catch (Exception ex)
            {
                ErrorMsg = ex.Message;
                return false;
            }

            return true;
        }

        /// <summary>
        /// getting the number of rows in separate column
        /// </summary>
        /// <param name="ErrorMsg"></param>
        /// <param name="RowCount"></param>
        /// <param name="ColumnIndex"></param>
        /// <returns></returns>
        public bool GetRowNumberInColumn(out string ErrorMsg, out int RowCount, int ColumnIndex)
        {

            ErrorMsg = string.Empty;
            RowCount = 0;

            try
            {

                Excel.Range xlRange = m_WorkSheet.UsedRange;
                RowCount = xlRange.Rows.Count;

            }

            catch (Exception ex)
            {
                ErrorMsg = ex.Message;
                return false;
            }

            return true;

        }

        /// <summary>
        /// saving the file
        /// </summary>
        /// <param name="ErrorMsg"></param>
        /// <returns></returns>
        public bool Save(out string ErrorMsg)
        {
            ErrorMsg = string.Empty;

            try
            {

                m_Workbook.SaveCopyAs(@m_strOutputPath);

            }

            catch (Exception ex)
            {
                ErrorMsg = ex.Message;
                return false;
            }

            return true;
        }

        /// <summary>
        /// closing an Excel application
        /// </summary>
        /// <param name="ErrorMsg"></param>
        /// <returns></returns>
        public bool Close(out string ErrorMsg)
        {
            ErrorMsg = string.Empty;

            try
            {

                m_Workbook.Close(false, m_MissingObject, m_MissingObject);

                m_Application.Quit();

                System.Runtime.InteropServices.Marshal.ReleaseComObject(m_Application);

                m_Application = null;
                m_Workbook = null;
                m_WorkSheet = null;

                System.GC.Collect();

            }
            catch (Exception ex)
            {
                ErrorMsg = ex.Message;
                return false;
            }

            return true;
        }

    }
}
