﻿using System;

namespace CoordsFromGoogle
{
    public class Config
    {        
        public string inputPath = "";
        public string outputPath = "";
        public int countOfCurrentRow = 1;        
        public string[] languages = { "ru", "uk", "en"};
        public int idOfCurrentLanguage = 0;
        public string pathToCurrentConfig = "";
        public DateTime lastRun = DateTime.Now;
    }
}
