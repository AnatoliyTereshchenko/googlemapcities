﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CoordsFromGoogle
{
    
    public partial class MainForm : Form
    {
        ChildForm FormForConfig = new ChildForm();
        private BusinessLogic Logic;
        private Config Configuration;

        public MainForm()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            using (ChildForm ChildForm = new ChildForm())
            {
                ChildForm.ShowDialog(this);
            }
                      
        }

        private void button2_Click(object sender, EventArgs e)
        {
            
            OpenFileDialog Open = new OpenFileDialog();
            Open.DefaultExt = "xml";
            Open.Filter = "XML Files|*.xml";          
            if (Open.ShowDialog() == DialogResult.OK)
            {
                ConfigSerializer ser = new ConfigSerializer();

                Configuration = ser.ConfigurationGetter(Open.FileName);
                Start_button.Enabled = true;
            }
            StartSession_Button.Enabled = false;
            LoadSession_Button.Enabled = false;
            logListBox.Items.Add("Config Loaded");
        }

        public void GetConfigFile(Config generatedFromDialog, string pathToSave)
        {
            ConfigSerializer ser = new ConfigSerializer();
            generatedFromDialog.pathToCurrentConfig = pathToSave;
            ser.Serialize(generatedFromDialog);
            Configuration = generatedFromDialog;
            StartSession_Button.Enabled = false;
            LoadSession_Button.Enabled = false;
            logListBox.Items.Add("Config Created");
            Start_button.Enabled = true;
        }

        private void Start_button_Click(object sender, EventArgs e)
        {
            Task t = Task.Factory.StartNew(() =>
            {
              while (true)
            {
                Thread.Sleep(500);
                Application.DoEvents();
            }
            });
            var worker = new BackgroundWorker();
            worker.DoWork += new DoWorkEventHandler(worker_DoWork);
            worker.RunWorkerAsync();
            Start_button.Enabled = false;
            
           
        }

        private void worker_DoWork(object sender, DoWorkEventArgs e)
        {
            Logic = new BusinessLogic(Configuration);
            Logic.ToLog += NewListEntryEventHandler;            
            Logic.Start();            
        }

        private void NewListEntryEventHandler(object sender, LogToListEventArgs e)
        {
            if (logListBox.InvokeRequired)
            {
                Invoke((MethodInvoker)delegate
                {
                    NewListEntryEventHandler(sender, e);
                });
                return;
            }

            logListBox.Items.Add(e.Test);
        }
              
    }
}
