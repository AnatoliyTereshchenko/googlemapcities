﻿namespace CoordsFromGoogle
{
    partial class ChildForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.button_Input = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.Output = new System.Windows.Forms.Button();
            this.Save = new System.Windows.Forms.Button();
            this.button_SaveConfigPath = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // button_Input
            // 
            this.button_Input.Location = new System.Drawing.Point(13, 13);
            this.button_Input.Name = "button_Input";
            this.button_Input.Size = new System.Drawing.Size(94, 23);
            this.button_Input.TabIndex = 0;
            this.button_Input.Text = "Select Input File";
            this.button_Input.UseVisualStyleBackColor = true;
            this.button_Input.Click += new System.EventHandler(this.button1_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(113, 18);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(0, 13);
            this.label1.TabIndex = 1;
            // 
            // Output
            // 
            this.Output.Location = new System.Drawing.Point(13, 55);
            this.Output.Name = "Output";
            this.Output.Size = new System.Drawing.Size(94, 23);
            this.Output.TabIndex = 2;
            this.Output.Text = "Select Output Folder";
            this.Output.UseVisualStyleBackColor = true;
            this.Output.Click += new System.EventHandler(this.Output_Click);
            // 
            // Save
            // 
            this.Save.Enabled = false;
            this.Save.Location = new System.Drawing.Point(62, 138);
            this.Save.Name = "Save";
            this.Save.Size = new System.Drawing.Size(163, 23);
            this.Save.TabIndex = 3;
            this.Save.Text = "SAVE";
            this.Save.UseVisualStyleBackColor = true;
            this.Save.Click += new System.EventHandler(this.Save_Click);
            // 
            // button_SaveConfigPath
            // 
            this.button_SaveConfigPath.Location = new System.Drawing.Point(13, 100);
            this.button_SaveConfigPath.Name = "button_SaveConfigPath";
            this.button_SaveConfigPath.Size = new System.Drawing.Size(94, 23);
            this.button_SaveConfigPath.TabIndex = 4;
            this.button_SaveConfigPath.Text = "Path For Config";
            this.button_SaveConfigPath.UseVisualStyleBackColor = true;
            this.button_SaveConfigPath.Click += new System.EventHandler(this.button_SaveConfigPath_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(116, 64);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(0, 13);
            this.label2.TabIndex = 5;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(116, 105);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(0, 13);
            this.label3.TabIndex = 6;
            // 
            // ChildForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(301, 173);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.button_SaveConfigPath);
            this.Controls.Add(this.Save);
            this.Controls.Add(this.Output);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.button_Input);
            this.Name = "ChildForm";
            this.Text = "ChildForm";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button_Input;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button Output;
        private System.Windows.Forms.Button Save;
        private System.Windows.Forms.Button button_SaveConfigPath;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
    }
}