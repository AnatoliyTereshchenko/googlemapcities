﻿using System;
using System.IO;

namespace CoordsFromGoogle
{

    class BusinessLogic
    {
        HttpWorker httpWorker = new HttpWorker();
        Config Configuration = new Config();
        const int RowNumber = 2450;
        string GoogleResponse = null;
        string longtitude = null;
        string latitude = null;
        int totalcells;
        public bool jobIsDone = false;
        string outputPath = null;



        public BusinessLogic(Config inpConfig)
        {
            this.Configuration = inpConfig;
            ExcelInit();
        }

        public delegate void LoggerForForm(object sender,
        LogToListEventArgs args);

        public event LoggerForForm ToLog;


        public void ExcelInit()
        {
            outputPath = Path.Combine(Configuration.outputPath, "FullData_" +
                    Configuration.languages[Configuration.idOfCurrentLanguage].ToLower() + ".xlsx");
        }

        public void Start()
        {
            using (ExcellWorker excellReader = new ExcellWorker(Configuration.inputPath))
            {
                totalcells = excellReader.GetCellNumberInColumn(1);
            }
            int max = Configuration.countOfCurrentRow + RowNumber;

            if (totalcells > max)
            {
                ProcessDocuments(max);
            }
            else if (totalcells == max)
            {
                ProcessDocuments(max);
                if (Configuration.idOfCurrentLanguage + 1 < Configuration.languages.Length)
                {
                    if (Configuration.idOfCurrentLanguage + 1 < Configuration.languages.Length)
                    {
                        this.Configuration.idOfCurrentLanguage = Configuration.idOfCurrentLanguage + 1;
                    }
                    ConfigSerializer ser = new ConfigSerializer();
                    ser.Serialize(Configuration);
                    ExcelInit();
                    Start();
                }
            }
            else
            {
                ProcessDocuments(totalcells);
                if (Configuration.idOfCurrentLanguage + 1 < Configuration.languages.Length)
                {

                    this.Configuration.idOfCurrentLanguage = Configuration.idOfCurrentLanguage + 1;

                    ConfigSerializer ser = new ConfigSerializer();
                    ser.Serialize(Configuration);
                    ExcelInit();
                    Start();
                }
                else
                {
                    PushToLog("Input file is processed");
                    jobIsDone = true;
                }
            }



        }

        private void Wait()
        {
            int hour = 1000 * 60 * 60;
            double diffBetweenLastRun = (DateTime.Now - Configuration.lastRun).TotalHours;
            if (diffBetweenLastRun <= 24)
            {
                PushToLog("WaitFor " + Convert.ToInt32(24 - diffBetweenLastRun).ToString() + " hours");
                System.Threading.Thread.Sleep(hour * (24 - (int)Math.Round(diffBetweenLastRun)) + 100000);
                Wait();
            }
            else
            {
                ConfigSerializer ser = new ConfigSerializer();
                Configuration = ser.ConfigurationGetter(Configuration.pathToCurrentConfig);
                ExcelInit();
                PushToLog("Application started to work");
                Start();

            }

        }

        protected virtual void PushToLog(string test)
        {
            if (this.ToLog != null)
            {
                ToLog(this, new LogToListEventArgs(test));
            }
        }

        private void ProcessDocuments(int max)
        {



            for (int i = Configuration.countOfCurrentRow; i < max; i++)
            {
                System.Threading.Thread.Sleep(30000);

                //getting coordinates from file

                using (ExcellWorker excellReader = new ExcellWorker(Configuration.inputPath))
                {
                    if (!File.Exists(outputPath))
                    {
                        excellReader.FileForOutpoot(outputPath);
                        PushToLog(outputPath + " - file is created");
                    }
                    if (!String.IsNullOrEmpty(excellReader.GetCellValue(i, 1)))
                    {
                        try
                        {
                            latitude = excellReader.GetCellValue(i, 1);
                            longtitude = excellReader.GetCellValue(i, 2);
                        }
                        catch (Exception) { PushToLog("Failed to read from input excell"); }
                    }
                    else
                    {
                        PushToLog("Blank row in input file, end of document");
                        break;
                    }
                }

                // getting the response data
                GetInfoFromGoogle(0);

                //parse data
                XmlParser xmlPrs = new XmlParser();
                xmlPrs.ParseData(GoogleResponse);


                if (String.IsNullOrEmpty(xmlPrs.GetCountry()))
                {
                    
                    GetInfoFromGoogle(5);
                    xmlPrs.ParseData(GoogleResponse);

                    if (String.IsNullOrEmpty(xmlPrs.GetCountry()))
                    {                        
                        GetInfoFromGoogle(30);
                        xmlPrs.ParseData(GoogleResponse);

                        if (String.IsNullOrEmpty(xmlPrs.GetCountry()))
                        {                           
                            GetInfoFromGoogle(60);
                            xmlPrs.ParseData(GoogleResponse);

                            if (String.IsNullOrEmpty(xmlPrs.GetCountry()))
                            {
                                Configuration.countOfCurrentRow = i - 1;
                                Configuration.lastRun = DateTime.Now;
                                ConfigSerializer ser = new ConfigSerializer();
                                ser.Serialize(Configuration);
                                PushToLog("Go to Sleep due to  queue limits");
                                Wait();
                            }
                        }
                    }

                }
                using (ExcellWorker excellWriter = new ExcellWorker(outputPath))
                {
                    excellWriter.SetCellValue(xmlPrs.GetCountry(), i, 1);
                    excellWriter.SetCellValue(xmlPrs.GetLocality(), i, 2);
                    excellWriter.SetCellValue(xmlPrs.GetAdminLevel2(), i, 3);
                    excellWriter.SetCellValue(xmlPrs.GetAdminLevel1(), i, 4);
                    excellWriter.Save(Configuration.outputPath, Configuration.languages[Configuration.idOfCurrentLanguage]);
                    PushToLog(xmlPrs.GetCountry() + "( Lat-" + latitude +
                        "| Long-" + longtitude +
                        " )" + xmlPrs.GetLocality() + " is pushed to output excell");
                }

            }

        }

        private void GetInfoFromGoogle(int timeout)
        {
            if (timeout > 0)
            {
                PushToLog("quaue problem, trying again in " + timeout*3 + " seconds");
                System.Threading.Thread.Sleep(timeout * 3000);
            }
            httpWorker.SetLanguage(Configuration.languages[Configuration.idOfCurrentLanguage]);
            httpWorker.SetCoordinates(latitude, longtitude);
            this.GoogleResponse = httpWorker.GetHttpResponse();
        }



    }
}
