﻿namespace CoordsFromGoogle
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.StartSession_Button = new System.Windows.Forms.Button();
            this.LoadSession_Button = new System.Windows.Forms.Button();
            this.Start_button = new System.Windows.Forms.Button();
            this.logListBox = new System.Windows.Forms.ListBox();
            this.SuspendLayout();
            // 
            // StartSession_Button
            // 
            this.StartSession_Button.Location = new System.Drawing.Point(12, 12);
            this.StartSession_Button.Name = "StartSession_Button";
            this.StartSession_Button.Size = new System.Drawing.Size(116, 23);
            this.StartSession_Button.TabIndex = 0;
            this.StartSession_Button.Text = "Start New Session";
            this.StartSession_Button.UseVisualStyleBackColor = true;
            this.StartSession_Button.Click += new System.EventHandler(this.button1_Click);
            // 
            // LoadSession_Button
            // 
            this.LoadSession_Button.Location = new System.Drawing.Point(12, 41);
            this.LoadSession_Button.Name = "LoadSession_Button";
            this.LoadSession_Button.Size = new System.Drawing.Size(116, 23);
            this.LoadSession_Button.TabIndex = 1;
            this.LoadSession_Button.Text = "Load Previous Session";
            this.LoadSession_Button.UseVisualStyleBackColor = true;
            this.LoadSession_Button.Click += new System.EventHandler(this.button2_Click);
            // 
            // Start_button
            // 
            this.Start_button.Enabled = false;
            this.Start_button.Location = new System.Drawing.Point(12, 227);
            this.Start_button.Name = "Start_button";
            this.Start_button.Size = new System.Drawing.Size(116, 23);
            this.Start_button.TabIndex = 2;
            this.Start_button.Text = "Start Transformation";
            this.Start_button.UseVisualStyleBackColor = true;
            this.Start_button.Click += new System.EventHandler(this.Start_button_Click);
            // 
            // logListBox
            // 
            this.logListBox.FormattingEnabled = true;
            this.logListBox.Location = new System.Drawing.Point(148, 15);
            this.logListBox.Name = "logListBox";
            this.logListBox.Size = new System.Drawing.Size(491, 212);
            this.logListBox.TabIndex = 3;            
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(660, 262);
            this.Controls.Add(this.logListBox);
            this.Controls.Add(this.Start_button);
            this.Controls.Add(this.LoadSession_Button);
            this.Controls.Add(this.StartSession_Button);
            this.Name = "MainForm";
            this.Text = "CoordsToNames";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button StartSession_Button;
        private System.Windows.Forms.Button LoadSession_Button;
        private System.Windows.Forms.Button Start_button;
        private System.Windows.Forms.ListBox logListBox;
    }
}

