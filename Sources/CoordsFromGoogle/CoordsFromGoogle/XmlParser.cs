﻿using System;
using System.Xml.Linq;

namespace CoordsFromGoogle
{
    class XmlParser
    {

        private string m_admin_area_level_1 = null;
        private string m_admin_area_level_2 = null;
        private string m_locality = null;
        private string m_country = null;       
        XDocument xdoc = null;

        //class designer
        public XmlParser()
        {           
        }

        //parsing data from file
        public void ParseData(string Response)
        {

            xdoc = XDocument.Parse(Response);

            
            
            foreach (XElement adressElement in xdoc.Element("GeocodeResponse").Elements("result"))
            {

                XElement LocalizedNameElement = adressElement.Element("formatted_address");

                XElement TypeElement = adressElement.Element("type");

                XElement NameElement = adressElement.Element("address_component").Element("long_name");

                if (NameElement != null)
                {

                    if (TypeElement.Value == "administrative_area_level_1")
                        m_admin_area_level_1 = NameElement.Value;

                    if (TypeElement.Value == "administrative_area_level_2")
                        m_admin_area_level_2 = NameElement.Value;

                    if (TypeElement.Value == "locality")
                        m_locality = NameElement.Value;

                    if (TypeElement.Value == "country")
                        m_country = NameElement.Value;

                }


            }

        }

        //methods for getting separeted adress values

        public string GetAdminLevel1()
        {

            return m_admin_area_level_1;

        }

        public string GetAdminLevel2()
        {

            return m_admin_area_level_2;

        }

        public string GetLocality()
        {

            return m_locality;

        }

        public string GetCountry()
        {

            return m_country;

        }

    }
}
