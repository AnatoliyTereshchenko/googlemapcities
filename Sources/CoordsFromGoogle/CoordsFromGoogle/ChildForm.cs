﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CoordsFromGoogle
{
    public partial class ChildForm : Form
    {
        Config NewConfiguration = new Config();
        private bool outputSelected = false;
        private bool inputSelected = false;
        private bool pathSelected = false;
        private string PathToSaveConfig;
        

        public ChildForm()
        {
            InitializeComponent();
        }

        private void Output_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog GetOutput = new FolderBrowserDialog();
            GetOutput.ShowNewFolderButton = true;
            if (GetOutput.ShowDialog() == DialogResult.OK &&  !String.IsNullOrEmpty(GetOutput.SelectedPath))
            {
                NewConfiguration.outputPath = GetOutput.SelectedPath;
                label2.Text = GetOutput.SelectedPath;
                outputSelected = true;
            }
            CheckState();
        }

       private void CheckState()
        {
            if (outputSelected && inputSelected && pathSelected)
            {
                Save.Enabled = true;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            OpenFileDialog Open = new OpenFileDialog();
            Open.Filter = "Excel Files (*.xlsx)|*.xlsx";
            if (Open.ShowDialog() == DialogResult.OK)
            {
                NewConfiguration.inputPath = Open.FileName;
                label1.Text = Open.FileName;
                inputSelected = true;
            }
            CheckState();
        }

        private void Save_Click(object sender, EventArgs e)
        {
            MainForm parent = (MainForm)this.Owner;
            parent.GetConfigFile(NewConfiguration, PathToSaveConfig);
            this.Close();
        }

        private void button_SaveConfigPath_Click(object sender, EventArgs e)
        {
            SaveFileDialog save = new SaveFileDialog
            {
                DefaultExt = ".xml",
                FileName = "configForGoogleCoords"
            };
            if (save.ShowDialog() == DialogResult.OK)
            {
                NewConfiguration.pathToCurrentConfig = save.FileName;
                PathToSaveConfig = save.FileName;
                label3.Text = save.FileName;
                pathSelected = true;
            }
            CheckState();
        }
    }
}
