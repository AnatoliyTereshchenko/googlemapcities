﻿using System;
using System.IO;
using System.Xml;
using System.Xml.Serialization;

namespace CoordsFromGoogle
{
    class ConfigSerializer 
    {
        public void Serialize(Config InputConfig)
        {            
            XmlSerializer xsSubmit = new XmlSerializer(typeof(Config));            
            var xml = "";
            using (var sww = new StringWriter())
            {
                using (XmlWriter writer = XmlWriter.Create(sww))
                {
                    xsSubmit.Serialize(writer, InputConfig);
                    xml = sww.ToString();
                    XmlDocument xdoc = new XmlDocument();
                    xdoc.LoadXml(xml);
                    string adder = DateTime.Now.ToShortDateString() + "_"+ DateTime.Now.ToShortTimeString().Replace(":",".");
                    xdoc.Save(InputConfig.pathToCurrentConfig);
                }
            }        
        }

        public Config ConfigurationGetter(string path)
        {

            XmlSerializer ser = new XmlSerializer(typeof(Config));
            return (Config)ser.Deserialize(new StreamReader(path));
        }
    }
}
