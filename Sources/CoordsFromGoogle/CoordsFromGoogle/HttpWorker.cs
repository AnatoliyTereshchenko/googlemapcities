﻿using System.Text;
using System.Net;
using System.IO;

//This file was created by Ivan Mazur in April 2015////////////////////////////////////////////////// 

namespace CoordsFromGoogle
{
    class HttpWorker
    {
        private string m_language = null;
        private string m_longtitude = null;
        private string m_latitude = null;

        //setting the coordinates
        public void SetCoordinates(string latitude, string longtitude)
        {

            m_latitude = latitude;
            m_longtitude = longtitude;

        }

        //setting language (for example *ru*)
        public void SetLanguage(string language)
        {

            m_language = language;

        }

        //pushing the request and returning the response
        public string GetHttpResponse()
        {
            string main_response = null;

            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(SetRequest());

            request.MaximumAutomaticRedirections = 1;
            request.MaximumResponseHeadersLength = 1;

            request.Credentials = CredentialCache.DefaultCredentials;
            HttpWebResponse response = (HttpWebResponse)request.GetResponse();

            Stream receiveStream = response.GetResponseStream();

            StreamReader readStream = new StreamReader(receiveStream, Encoding.UTF8);

            main_response = readStream.ReadToEnd();

            response.Close();
            readStream.Close();

            return main_response;
        }

        //make some settings for request
        private string SetRequest()
        {
            string request = null;

            string sample = "https://maps.googleapis.com/maps/api/geocode/xml?latlng=";

            sample += m_longtitude;
            sample += ",";
            sample += m_latitude;
            sample += "&language=";
            sample += m_language;
            sample += "&sensor=true_or_false";
            sample += "&key=" + "AIzaSyDGmhwmc4WvFKsmr-GJxq-01selM72NgD4";

            request = sample;

            return request;
        }


    }
}
