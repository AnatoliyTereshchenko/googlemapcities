﻿using System;
using System.IO;
using Excel = Microsoft.Office.Interop.Excel;

namespace CoordsFromGoogle 
{
    class ExcellWorker : System.IDisposable
    {
        private Excel._Application m_Application = null;
        private Excel.Workbook m_Workbook = null;
        private Excel.Worksheet m_WorkSheet = null;
        private object m_MissingObject = System.Reflection.Missing.Value;
        private object strOutputPath;


        //class designer



        public ExcellWorker(string path)
        {
            
            object PathToFileObject = path;

            m_Application = new Excel.Application();
            m_Workbook = m_Application.Workbooks.Add(PathToFileObject);
            m_Application.Visible = false;
            m_WorkSheet = (Excel.Worksheet)m_Workbook.Worksheets.get_Item(1);

        }

        public void FileForOutpoot(string path)
        {
            var app = new Excel.Application();
            var wb = app.Workbooks.Add();
            wb.SaveAs(path);
            wb.Close();

            app.Workbooks.Close();
            app.Quit();
            System.Runtime.InteropServices.Marshal.ReleaseComObject(app);
            app = null;
            GC.Collect();
            GC.WaitForPendingFinalizers();
            GC.Collect();
        }

        //the option of excel application visibility
        public bool Visible
        {

            get { return m_Application.Visible; }
            set { m_Application.Visible = value; }

        }

        //setting value for excel file`s cell with such indexes
        public void SetCellValue(string NewValue, int RowIndex, int ColumnIndex)
        {

            m_WorkSheet.Cells[RowIndex, ColumnIndex] = NewValue;

        }

        //getting cell value from excel file with such indexes
        public string GetCellValue(int RowIndex, int ColumnIndex)
        {

            string RequiredValue = null;

            try
            {
                if (m_Application.Cells[RowIndex, ColumnIndex].Value != null)
                {

                    RequiredValue = m_Application.Cells[RowIndex, ColumnIndex].Value.ToString();

                    return RequiredValue;
                }
            }
            catch (Exception)
            {
                return "";
                throw new Exception();
            }
            return "";
        }

        //getting the cells number in this column
        public int GetCellNumberInColumn(int ColumnIndex)
        {
            
           
                int RowCount = m_WorkSheet.UsedRange.Count;
                return RowCount;
                

        }

        //saving the file
        public void Save(string path, string language)
        {
            strOutputPath = System.IO.Path.Combine(path, "FullData_"+language.ToLower()+".xlsx");           
            m_Workbook.SaveCopyAs(@strOutputPath);
            //m_Workbook.Close(true, @strOutputPath, m_MissingObject);
        }

        //closing and desposing all used data 
        public void Close()
        {           
            m_Workbook.Close(false, @strOutputPath, m_MissingObject);
            m_Application.Quit();
            System.Runtime.InteropServices.Marshal.ReleaseComObject(m_Application);
            m_Application = null;
            GC.Collect();
            GC.WaitForPendingFinalizers();
            GC.Collect();
            GC.WaitForPendingFinalizers();

        }
               

        // This code added to correctly implement the disposable pattern.
        public void Dispose()
        {
            if (m_Application != null)
            {
                Close();
            }
        }
        
    }
}
