﻿using System;

namespace CoordsFromGoogle
{
    public class LogToListEventArgs : EventArgs
    {
        private readonly string test;

        public LogToListEventArgs(string test)
        {
            this.test = test;
        }

        public string Test
        {
            get { return test; }
        }
    }
}
